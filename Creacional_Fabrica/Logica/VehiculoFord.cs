﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    public class VehiculoFord : IVehiculoMarca
    {
        //Variables de clase
        private List<Vehiculo> vehiculos;
        private string marca;

        //Constructor
        public VehiculoFord(string Marca)
        {
            //Crear modelo para devolver vehiculos
            List<Vehiculo> vehiculo = new List<Vehiculo>();
            vehiculo.Add(new Vehiculo() { Referencia = "Escape", Motor = "MT", Puertas = "4" });
            vehiculo.Add(new Vehiculo() { Referencia = "F150", Motor = "AT", Puertas = "4" });
            vehiculo.Add(new Vehiculo() { Referencia = "Mustang", Motor = "AT", Puertas = "2" });

            //Inicializar variables de clase
            this.vehiculos = vehiculo;
            this.marca = Marca;
        }

        //Implementar método de la interfaz
        public void GetVehiculos()
        {
            Console.WriteLine($"Inventario {marca}");
            foreach (var item in vehiculos)
            {
                Console.WriteLine($"Referencia: {item.Referencia} - Motor: {item.Motor} - # Puertas: {item.Puertas}");
            }
            Console.WriteLine(Environment.NewLine);
        }
    }
}
