﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    public class VehiculoNissan : IVehiculoMarca
    {
        //Variables de clase
        private List<Vehiculo> vehiculos;
        private string marca;

        //Constructor
        public VehiculoNissan(string Marca)
        {
            //Crear modelo para devolver vehiculos
            List<Vehiculo> vehiculo = new List<Vehiculo>();
            vehiculo.Add(new Vehiculo() { Referencia = "March", Motor = "MT", Puertas = "4" });
            vehiculo.Add(new Vehiculo() { Referencia = "Kicks", Motor = "AT", Puertas = "4" });
            vehiculo.Add(new Vehiculo() { Referencia = "Versa", Motor = "AT", Puertas = "2" });

            //Inicializar variables de clase
            this.vehiculos = vehiculo;
            this.marca = Marca;
        }

        //Implementar método de la interfaz
        public void GetVehiculos()
        {
            Console.WriteLine($"Inventario {marca}");
            foreach (var item in vehiculos)
            {
                Console.WriteLine($"Referencia: {item.Referencia} - Motor: {item.Motor} - # Puertas: {item.Puertas}");
            }
            Console.WriteLine(Environment.NewLine);
        }
    }
}
