﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    public class VehiculoNA : IVehiculoMarca
    {
        //Variables de clase
        private string marca;

        //Constructor
        public VehiculoNA(string Marca)
        {
            //Inicializar variables de clase
            this.marca = Marca;
        }

        //Implementar método de la interfaz
        public void GetVehiculos()
        {
            Console.WriteLine($"No existe el inventario solicitado para {marca}");
            Console.WriteLine(Environment.NewLine);
        }
    }
}
