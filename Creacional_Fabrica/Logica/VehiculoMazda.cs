﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    public class VehiculoMazda : IVehiculoMarca
    {
        //Variables de clase
        private List<Vehiculo> vehiculos;
        private string marca;

        //Constructor
        public VehiculoMazda(string Marca)
        {
            //Crear modelo para devolver vehiculos
            List<Vehiculo> vehiculo = new List<Vehiculo>();
            vehiculo.Add(new Vehiculo() { Referencia = "CX-30", Motor = "AT", Puertas = "4" });
            vehiculo.Add(new Vehiculo() { Referencia = "CX-3", Motor = "MT", Puertas = "4" });
            vehiculo.Add(new Vehiculo() { Referencia = "CX-5", Motor = "AT", Puertas = "4" });

            //Inicializar variables de clase
            this.vehiculos = vehiculo;
            this.marca = Marca;
        }

        //Implementar método de la interfaz
        public void GetVehiculos()
        {
            Console.WriteLine($"Inventario {marca}");
            foreach (var item in vehiculos)
            {
                Console.WriteLine($"Referencia: {item.Referencia} - Motor: {item.Motor} - # Puertas: {item.Puertas}");
            }
            Console.WriteLine(Environment.NewLine);
        }
    }
}
