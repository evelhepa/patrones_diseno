﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    public class Vehiculo
    {
        public string Referencia { get; set; }
        public string Motor { get; set; }
        public string Puertas { get; set; }
    }
}
