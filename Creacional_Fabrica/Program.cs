﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciar patrón
            VehiculoFabrica fabrica = new VehiculoFabrica();

            //Ejecutar patrón
            IVehiculoMarca vehiculo1 = fabrica.vehiculoMarca("Mazda");
            vehiculo1.GetVehiculos();

            IVehiculoMarca vehiculo2 = fabrica.vehiculoMarca("Nissan");
            vehiculo2.GetVehiculos();

            IVehiculoMarca vehiculo3 = fabrica.vehiculoMarca("Ford");
            vehiculo3.GetVehiculos();

            IVehiculoMarca vehiculo4 = fabrica.vehiculoMarca("Chevorlet");
            vehiculo4.GetVehiculos();

            Console.ReadLine();
        }
    }
}
