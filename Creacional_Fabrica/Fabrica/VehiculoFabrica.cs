﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creacional_Fabrica
{
    public class VehiculoFabrica
    {
        //Implementación patrón creacional fabrica
        public IVehiculoMarca vehiculoMarca(string Marca)
        {
            switch (Marca.ToLower())
            {
                case "mazda" :
                    return new VehiculoMazda(Marca);
                case "nissan":
                    return new VehiculoNissan(Marca);
                case "ford":
                    return new VehiculoFord(Marca);
                default:
                    return new VehiculoNA(Marca);
            }
        }
    }
}
