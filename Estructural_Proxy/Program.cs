﻿using Estructural_Proxy.Entidades;
using Estructural_Proxy.Interfaz;
using Estructural_Proxy.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructural_Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            Asignatura model = new Asignatura()
            {
                NombreAsignatura = "Arquitectura de Software",
                IdEstudiante = 1152694655,
                NombreEstudiante = "Evelin Henao",
                Nota = 4.0
            };

            AsignaturaProxy asignatura = new AsignaturaProxy();
            model = asignatura.cambiarNota(model, 4.5);

            asignatura.mostrarNota(model);

            Console.ReadLine();
        }
    }
}
