﻿using Estructural_Proxy.Entidades;
using Estructural_Proxy.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructural_Proxy.Logica
{
    public class OptAsignatura : IAsignatura
    {
        public Asignatura cambiarNota(Asignatura model, double ultimaNota)
        {
            model.Nota = (model.Nota + ultimaNota) / 2;
            return model;
        }

        public void mostrarNota(Asignatura model)
        {
            Console.WriteLine($"La nota final para el estudiante: {model.NombreEstudiante} en la asignatura {model.NombreAsignatura} es: {model.Nota.ToString()}");
        }
    }
}
