﻿using Estructural_Proxy.Entidades;
using Estructural_Proxy.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructural_Proxy.Proxy
{
    public class AsignaturaProxy
    {
        private OptAsignatura asignatura;

        public Asignatura cambiarNota(Asignatura model, double ultimaNota)
        {
            asignatura = new OptAsignatura();
            return asignatura.cambiarNota(model, ultimaNota);
        }

        public void mostrarNota(Asignatura model)
        {
            asignatura.mostrarNota(model);
        }
    }
}
