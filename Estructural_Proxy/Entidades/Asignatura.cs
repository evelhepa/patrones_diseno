﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructural_Proxy.Entidades
{
    public class Asignatura
    {
        public int IdEstudiante { get; set; }
        public string NombreEstudiante { get; set; }
        public string NombreAsignatura { get; set; }
        public double Nota { get; set; }
    }
}
