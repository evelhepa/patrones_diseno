﻿using Estructural_Proxy.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructural_Proxy.Interfaz
{
    public interface IAsignatura
    {
        Asignatura cambiarNota(Asignatura asignatura, double ultimaNota);
        void mostrarNota(Asignatura asignatura);
        
    }
}
