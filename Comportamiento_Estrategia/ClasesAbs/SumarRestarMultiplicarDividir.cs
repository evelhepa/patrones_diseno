﻿using Comportamiento_Estrategia.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comportamiento_Estrategia.ClasesAbs
{
    public abstract class SumarRestarMultiplicarDividir : IOperaciones
    {
        //Implementación de métodos en la interfaz
        public void Ejecutar()
        {
            Sumar();
            Restar();
            Multiplicar();
            Dividir();
        }

        public abstract void Sumar();
        public abstract void Restar();
        public abstract void Multiplicar();
        public abstract void Dividir();
    }
}

