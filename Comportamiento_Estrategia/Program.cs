﻿using Comportamiento_Estrategia.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comportamiento_Estrategia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ejecutar suma y resta
            Console.WriteLine("Ejecutar operaciones Suma y Resta");
            ContextoEstrategia context = new ContextoEstrategia(new OpSumaResta(10, 5));
            context.Ejecutar();
            Console.WriteLine();

            //Ejecutar suma, resta, multiplicación y división
            Console.WriteLine("Ejecutar operaciones Suma, Resta, Multiplicación y División");
            ContextoEstrategia context1 = new ContextoEstrategia(new OpSumaRestaMultiDiv(10, 5));
            context1.Ejecutar();

            Console.ReadLine();
        }
    }
}
