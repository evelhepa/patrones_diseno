﻿using Comportamiento_Estrategia.ClasesAbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comportamiento_Estrategia.Logica
{
    public class OpSumaResta : SumarRestar
    {
        private int varA;
        private int varB;
        public OpSumaResta(int var1, int var2)
        {
            this.varA = var1;
            this.varB = var2;
        }

        public override void Sumar()
        {
            var suma = varA + varB;
            Console.WriteLine($"La suma de ({varA} + {varB}) es equivalente a {suma}");
        }

        public override void Restar()
        {
            var resta = varA - varB;
            Console.WriteLine($"La resta de ({varA} - {varB}) es equivalente a {resta}");
        }
    }
}
