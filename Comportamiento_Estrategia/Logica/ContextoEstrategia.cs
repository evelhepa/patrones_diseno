﻿using Comportamiento_Estrategia.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comportamiento_Estrategia.Logica
{
    public class ContextoEstrategia
    {
        //Instanciar la interfaz para poderla utilizar
        private IOperaciones operacion;

        public ContextoEstrategia(IOperaciones operacion)
        {
            this.operacion = operacion;
        }

        public void Ejecutar()
        {
            this.operacion.Ejecutar();
        }
    }
}
